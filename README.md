###### Keycloak GA4GH Passport Broker for the EGA Permissions API provider.

The provider is implemented as a Keyclock OpenID Connect Protocol Mapper
that "maps" obtained via EGA REST Permissions API GA4GH Passport Visas into the 
OpenID Connect GA4GH Passport scope.  

Manual maven build:
```shell
>git clone https://gitlab.bsc.es/inb/ga4gh/ga4gh-passport/permission-api-protocol-mapper.git
>cd permission-api-protocol-mapper
>mvn install
```

The provider is also built automatically by the continuous integration and can be found in the INB/BSC maven repository:  

https://inb.bsc.es/maven/es/bsc/inb/ga4gh/passport/keycloak/permissions-api-protocol-mapper/0.0.1/permissions-api-protocol-mapper-0.0.1.jar

Because the Permission API API is most probably protected by its own OIDC Identity Provider,
the protocol mapper exchanges identities between local Keycloak user and his/her external one.
To do so, previos identity linking must be performed.

This jar should be put into the $KEYCLOAK_HOME/providers/ directory.  
This should be enough as both WildFly and Quarkus implementations look into this directories for
extentions.  

*standalone.xml* file should be already configured to use this directory:

~~~xml
<subsystem xmlns="urn:jboss:domain:keycloak-server:1.1">
  <providers>
    <provider>
      classpath:${jboss.home.dir}/providers/*
    </provider>
  </providers>
~~~

There are several additional ways to install this mapper into the WildFly-based Keycloak (prior version 17):  

1. The mapper may be installed as a WildFly *module*:  
This way the directory 
$KEYCLOAK_HOME/modules/system/layers/keycloak/es/bsc/inb/keycloak/permissions-api-protocol-mapper/main
should be created and *module.xml* file must be provided along the mapper jar file.

2. The mapper may be put into the $KEYCLOAK_HOME/stanalone/deployment directory.




