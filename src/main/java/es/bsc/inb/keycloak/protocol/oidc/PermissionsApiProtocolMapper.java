/*
 * Copyright (C) 2021 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package es.bsc.inb.keycloak.protocol.oidc;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.keycloak.TokenCategory;
import static org.keycloak.broker.oidc.AbstractOAuth2IdentityProvider.FEDERATED_REFRESH_TOKEN;
import org.keycloak.broker.oidc.OIDCIdentityProvider;
import org.keycloak.broker.provider.IdentityProvider;
import static org.keycloak.broker.provider.IdentityProvider.FEDERATED_ACCESS_TOKEN;
import org.keycloak.crypto.SignatureProvider;
import org.keycloak.crypto.SignatureSignerContext;
import org.keycloak.events.Details;
import org.keycloak.jose.jws.JWSBuilder;
import org.keycloak.jose.jws.JWSInput;
import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.FederatedIdentityModel;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperContainerModel;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.ProtocolMapperConfigException;
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper;
import org.keycloak.protocol.oidc.mappers.UserInfoTokenMapper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.JsonWebToken;
import org.keycloak.services.Urls;
import org.keycloak.services.resources.IdentityBrokerService;
import org.keycloak.util.JsonSerialization;
import org.keycloak.util.TokenUtil;
import org.keycloak.utils.RoleResolveUtil;

/**
 * @author Dmitry Repchevsky
 */

public class PermissionsApiProtocolMapper extends AbstractOIDCProtocolMapper 
        implements UserInfoTokenMapper {

    public static final String PROVIDER_ID = "permissions-api-ga4gh-passport-mapper";
    public static final String PROVIDER_DISPLAY_TYPE = "GA4GH Passport Scope";
    public static final String PROVIDER_HELP_TEXT = "Permissions API based GA4GH Passport mapper";
    
    public final static String PERMISSIONS_API_ENDPOINT_PROPERTY_NAME = "permissions-api-endpoint-uri";
    public final static String PERMISSIONS_API_ENDPOINT_PROPERTY_LABEL = "Permissions API endpoint URI";
    public final static String PERMISSIONS_API_ENDPOINT_PROPERTY_TITLE = "Permissions API endpoint location string";
    
    public final static String PERMISSIONS_API_PLAIN_VISAS_PROPERTY_NAME = "permissions-api-plain-visas";
    public final static String PERMISSIONS_API_PLAIN_VISAS_PROPERTY_LABEL = "GH4GH PLAIN Visas";
    public final static String PERMISSIONS_API_PLAIN_VISAS_PROPERTY_TITLE = "Use GA4GH PLAIN Visas (default JWT format)";

    public final static String PERMISSIONS_API_EXT_IDP_PROPERTY_NAME = "permissions-api-external_idp";
    public final static String PERMISSIONS_API_EXT_IDP_PROPERTY_LABEL = "External IDP";
    public final static String PERMISSIONS_API_EXT_IDP_PROPERTY_TITLE = "Use an external IDP for the Permissions API";

    private final static List<ProviderConfigProperty> config_properties;

    static {
        final ProviderConfigProperty endpoint_propery = 
                new ProviderConfigProperty(
                        PERMISSIONS_API_ENDPOINT_PROPERTY_NAME, 
                        PERMISSIONS_API_ENDPOINT_PROPERTY_LABEL, 
                        PERMISSIONS_API_ENDPOINT_PROPERTY_TITLE,
                        ProviderConfigProperty.STRING_TYPE, 
                        null);
        
        final ProviderConfigProperty visas_format =
                new ProviderConfigProperty(
                        PERMISSIONS_API_PLAIN_VISAS_PROPERTY_NAME, 
                        PERMISSIONS_API_PLAIN_VISAS_PROPERTY_LABEL, 
                        PERMISSIONS_API_PLAIN_VISAS_PROPERTY_TITLE,
                        ProviderConfigProperty.BOOLEAN_TYPE,
                        Boolean.FALSE);

        final ProviderConfigProperty ext_idp =
                new ProviderConfigProperty(
                        PERMISSIONS_API_EXT_IDP_PROPERTY_NAME, 
                        PERMISSIONS_API_EXT_IDP_PROPERTY_LABEL, 
                        PERMISSIONS_API_EXT_IDP_PROPERTY_TITLE,
                        ProviderConfigProperty.STRING_TYPE,
                        null);

        config_properties = List.of(endpoint_propery, visas_format, ext_idp);
    }

    @Override
    public String getDisplayType() {
        return PROVIDER_DISPLAY_TYPE;
    }

    @Override
    public String getDisplayCategory() {
        return TOKEN_MAPPER_CATEGORY;
    }

    @Override
    public String getHelpText() {
        return PROVIDER_HELP_TEXT;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return config_properties;

    }

    @Override
    public int getPriority() {
        return 1000;
    }
 
    @Override
    public void validateConfig(KeycloakSession ksession, RealmModel realm, ProtocolMapperContainerModel client, ProtocolMapperModel model) throws ProtocolMapperConfigException {
        final String endpoint = model.getConfig().get(PERMISSIONS_API_ENDPOINT_PROPERTY_NAME);

        try {
            new URL(endpoint);
        } catch (MalformedURLException ex) {
            Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.FINEST, null, ex);
            throw new ProtocolMapperConfigException("invalid endpoint url", "{0}", ex.getMessage());
        }
        
        final String ext_ipp_alias = model.getConfig().get(PERMISSIONS_API_EXT_IDP_PROPERTY_NAME);
        if (ext_ipp_alias != null && !ext_ipp_alias.isEmpty()) {
            try (Stream<IdentityProviderModel> providers = realm.getIdentityProvidersStream()) {
                if (providers.filter(p -> ext_ipp_alias.equals(p.getAlias())).count() == 0) {
                    throw new ProtocolMapperConfigException("invalid external idp: " + ext_ipp_alias, "{0}");                
                }
            }
        }
    }
    
    @Override
    public AccessToken transformUserInfoToken(AccessToken token, ProtocolMapperModel model, 
            KeycloakSession ksession, UserSessionModel usession, ClientSessionContext ctx) {
        
        final UserModel user = usession.getUser();
        if (user != null) {
            String endpoint = model.getConfig().get(PERMISSIONS_API_ENDPOINT_PROPERTY_NAME);
            if (endpoint != null) {
                final Object plain = model.getConfig().get(PERMISSIONS_API_PLAIN_VISAS_PROPERTY_NAME);
                final String format = (plain != null && Boolean.parseBoolean(plain.toString())) ? "PLAIN" : "JWT";
                final String signed_token = getToken(model, ksession, usession, ctx);
                if (signed_token != null) {
                    final List visas = getPermissions(endpoint + "/me/permissions?format=" + format, signed_token);
                    if (visas != null) {
                        final Map<String, Object> claims = token.getOtherClaims();
                        final Object claim = claims.get("ga4gh_passport_v1");
                        if (claim instanceof List) {
                            for (Object visa : visas) {
                                ((List)claim).add(visa);
                            }                            
                        } else {
                            if (claim != null) {
                                visas.add(claim);
                            }
                            claims.put("ga4gh_passport_v1", visas);
                        }
                        return token;
                    }
                }
            }
        }
        return token;

    }

    private String getToken(ProtocolMapperModel model, 
            KeycloakSession ksession, UserSessionModel usession, ClientSessionContext ctx) {

        final String ext_idp_alias = model.getConfig().get(PERMISSIONS_API_EXT_IDP_PROPERTY_NAME);
        if (ext_idp_alias != null) {
            // exchange token to the external one
            OIDCIdentityProvider provider = (OIDCIdentityProvider)IdentityBrokerService.getIdentityProvider(ksession, usession.getRealm(), ext_idp_alias);
            
            if (provider.getConfig().isStoreToken()) {
                final FederatedIdentityModel fimodel = ksession.users().getFederatedIdentity(
                        usession.getRealm(), usession.getUser(), provider.getConfig().getAlias());
                
                if (fimodel != null) {
                    String ext_token = fimodel.getToken();
                    if (ext_token == null) {
                        String broker_id = usession.getNote(Details.IDENTITY_PROVIDER);
                        if (broker_id == null) {
                            usession.getNote(IdentityProvider.EXTERNAL_IDENTITY_PROVIDER);
                        }
                        if (broker_id != null && broker_id.equals(provider.getConfig().getAlias())) {
                            ext_token = usession.getNote(FEDERATED_ACCESS_TOKEN);
                        }                        
                    }
                    if (ext_token != null) {
                        try {
                            AccessTokenResponse resp = JsonSerialization.readValue(ext_token, AccessTokenResponse.class);
                            final String access_token = resp.getToken();
                            if (access_token != null) {
                                try {
                                    final JWSInput jws = new JWSInput(access_token);
                                    final JsonWebToken jwt = jws.readJsonContent(JsonWebToken.class);
                                    if (jwt.isActive()) {
                                        return access_token;
                                    }
                                } catch (Exception ex) {
                                    Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.INFO, "invalid access token", ex);
                                }
                            }
                            // no access token or expired
                            final String refresh_token = resp.getRefreshToken();
                            if (refresh_token != null) {
                                try {
                                    final JWSInput jws = new JWSInput(refresh_token);
                                    final JsonWebToken jwt = jws.readJsonContent(JsonWebToken.class);
                                    if (jwt.isActive()) {
                                        // this is a hack. we can't call protected exchangeSessionToken()
                                        usession.setNote(FEDERATED_REFRESH_TOKEN, refresh_token);
                                        ext_token = provider.refreshTokenForLogout(ksession, usession);
                                        resp = JsonSerialization.readValue(ext_token, AccessTokenResponse.class);
                                        return resp.getToken();
                                    }
                                } catch (Exception ex) {
                                    Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.INFO, "invalid refresh token", ex);
                                }
                            }
                            return null;
                        } catch (IOException ex) {
                            Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }

        final AccessToken access_token = new AccessToken()
            .subject(usession.getUser().getId())
            .issuer(Urls.realmIssuer(ksession.getContext().getUri().getBaseUri(), ksession.getContext().getRealm().getName()));

        access_token.addAudience(ksession.getContext().getClient().getClientId());
        access_token.issuedNow();
        access_token.exp(access_token.getIat() + 60L);
        access_token.type(TokenUtil.TOKEN_TYPE_BEARER);
        access_token.setAllowedOrigins(new HashSet(Arrays.asList("*")));

        // add audiences
        String client_id = ctx.getClientSession().getClient().getClientId();
        for (Map.Entry<String, AccessToken.Access> entry : RoleResolveUtil.getAllResolvedClientRoles(ksession, ctx).entrySet()) {
            if (!entry.getKey().equals(client_id)) {
                AccessToken.Access access = entry.getValue();
                if (access != null && access.getRoles() != null && !access.getRoles().isEmpty()) {
                    access_token.addAudience(entry.getKey());
                }
            }
        }

        final String alg = ksession.tokens().signatureAlgorithm(TokenCategory.ACCESS);
        final SignatureSignerContext signer = ksession.getProvider(SignatureProvider.class, alg).signer();

        return new JWSBuilder().type("JWT").jsonContent(access_token).sign(signer);
    }
    
    private List getPermissions(final String endpoint, final String token) {
        try {
            URI uri = URI.create(endpoint);
            loop:
            for (int i = 0; i < 10; i++) {
                final URL url = uri.toURL();
                HttpURLConnection con = null;
                try {
                    con = (HttpURLConnection)url.openConnection();

                    con.setReadTimeout(120000);
                    con.setConnectTimeout(300000);
                    con.setInstanceFollowRedirects(false);

                    con.addRequestProperty("Authorization", "Bearer " + token);
                    
                    try (BufferedInputStream in = new BufferedInputStream(con.getInputStream())) {
                        final int code = con.getResponseCode();
                        switch(code) {
                            case HttpURLConnection.HTTP_OK:          
                            case HttpURLConnection.HTTP_NOT_MODIFIED:
                                String text = new String(in.readAllBytes(), StandardCharsets.UTF_8);
                                return JsonSerialization.mapper.readValue(text, List.class);
                                
                            case HttpURLConnection.HTTP_MOVED_PERM:
                            case HttpURLConnection.HTTP_MOVED_TEMP:
                            case 307: // Temporary Redirect
                            case 308: // Permanent Redirect
                            case HttpURLConnection.HTTP_SEE_OTHER:
                                final String location = con.getHeaderField("Location");
                                if (location == null || location.isEmpty()) {
                                    break loop;
                                }
                                final URI redirect = URI.create(location);
                                uri = redirect.isAbsolute() ? redirect : uri.resolve(redirect);
                                break;
                            default:
                                Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.WARNING, "error from permissions server: " + code);
                                break loop;
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.WARNING, "error reading permissions", ex);
                    if (con != null) {
                        try (InputStream in = con.getErrorStream()) {}
                    }
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PermissionsApiProtocolMapper.class.getName()).log(Level.WARNING, "error reading permissions", ex);
        }

        return null;
    }
}
